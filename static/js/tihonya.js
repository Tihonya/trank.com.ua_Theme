﻿(function () {
    $(document).ready(initialNavbarLightingChanging());


    function initialNavbarLightingChanging() {
        var header = $('.home-page header');
        console.log(header);
        if (header.length > 0) {
            var offset = $('.container.width-1920').offset().top - header.outerHeight();
            var flag = false;

            $(window).scroll(function () {
                if (window.scrollY >= offset) {
                    if (!flag) {
                        flag = true;
                        header.addClass('header-light');
                    }
                } else {
                    if (flag) {
                        flag = false;
                        header.removeClass('header-light');
                    }
                }
                console.dir(header);
            });
        }
    }

}());